function isNotNumericValue(value) {
    return isNaN(value) || !isFinite(value);
  }

  var calc = new Vue({
    el: '#app',
    data: { x: 0, y: 0, lastResult: 0 , multi : 0, divi : 0, wat : 'wat'},
    computed: {
      result: function() {
        let x = parseFloat(this.x);
        if(isNotNumericValue(x)) {
        	return this.lastResult;
        }

        let y = parseFloat(this.y);
        if(isNotNumericValue(y)) {
        	return this.lastResult;
        }

        this.lastResult = x + y;

        return this.lastResult;
      },
      saveThings: function() {
				localStorage.setItem('xEntry', JSON.stringify(this.x));
				localStorage.setItem('yEntry', JSON.stringify(this.y));
				localStorage.setItem('lastResultEntry', JSON.stringify(this.lastResult));
				return 0;
      },
      listWat: function() {
				this.wat = "last multi: " + localStorage.getItem('multiEntry') +
				", last div: " + localStorage.getItem('divEntry') + "\n" +
				"X" + localStorage.getItem('xEntry') + " + Y" + localStorage.getItem('yEntry') +
				" = " + localStorage.getItem('lastResultEntry');
				return 0;
      },
      multiply: function() {
				this.multi = this.x * this.y;
				localStorage.setItem('multiEntry', JSON.stringify(this.multi));
				return this.multi;
	  	},
      division: function() {
				this.divi = this.x / this.y;
				localStorage.setItem('divEntry', JSON.stringify(this.divi));
				return this.divi;
	  	}
    }
  });