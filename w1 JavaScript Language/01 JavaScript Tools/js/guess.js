"use strict";
let targetNumber = Math.floor(Math.random() * 10) + 1;
let guesses = 0;


function init () {
	//console.log("You have to code this!");
	var val = document.getElementById("number");
	check (parseInt(val.value));
}

function check (value) {
	guesses += 1;
	if (value === targetNumber) {
		showWin();
	} else {
		if (guesses < 5){
			showError();
		} else {
			showLoss();
		}
	}
}

function showWin () {
	document.getElementById("result").innerHTML = "You win!";
	document.body.innerHTML = document.getElementById("result").innerHTML;
}

function showError () {
	var message = "";
	if (guesses === 4) {
		message = "YOUR FINAL GUESS, MORTAL! ";
	} else {
		message = (5 - guesses) + " GUESSES LEFT. ";
	}

	document.getElementById("result").innerHTML = "WROOOOOOONG! "
	+ message
	+ "Psst, " + targetNumber;
}

function showLoss () {
	document.getElementById("result").innerHTML = "YOUR TIME IS UP, YOU SORRY FOOL!";
	document.body.innerHTML = document.getElementById("result").innerHTML;
}

//init();
