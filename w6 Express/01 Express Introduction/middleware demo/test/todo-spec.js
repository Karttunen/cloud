
var frisby = require('frisby')

/*  // globalSetup defines any settigs used for ALL requests */
frisby.globalSetup({
  request: {
    headers: {'Authorization': 'Basic dGVzdHVzZXI6cDQ1NXcwcmQ=','Content-Type': 'application/json'}
  }
})

frisby.create('delete crap')
	.delete('https://cloud-programming-course-karttunen.c9users.io:8080/plop')
  .toss()

/* here is a simple automated API call making a GET request. We check the response code, one of the response headers and the content of the response body. After completing the test we call 'toss()' which moves the script to the next test. */
frisby.create('get specific')
  .get('https://cloud-programming-course-karttunen.c9users.io:8080/plop:herp')
  .toss()

/* in this second POST example we don't know precisely what values will be returned but we can check for the correct data types. Notice that the request body is passed as the second parameter and we need to pass a third parameter to indicate we are passing the data in json format. */
frisby.create('postage')
  .post('https://cloud-programming-course-karttunen.c9users.io:8080/plop:derp')
	.toss()

/* Since Frisby is built on the Jasmine library we can use any of the standard matchers by enclosing them in an anonymous function passed to the 'afterJSON()' method. */
frisby.create('basic get')
  .get('https://cloud-programming-course-karttunen.c9users.io:8080/plop')
  .toss()
