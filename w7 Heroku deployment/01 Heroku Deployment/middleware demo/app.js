var express = require('express'),
  path = require('path'),
  fs = require('fs'),
  compression = require('compression'),
  logger = require('morgan'),
  timeout = require('connect-timeout'),
  methodOverride = require('method-override'),
  responseTime = require('response-time'),
  favicon = require('serve-favicon'),
  serveIndex = require('serve-index'),
  vhost = require('vhost'),
  busboy = require('connect-busboy'),
  errorhandler = require('errorhandler');
	//testing = require('todo_spec');

var verbose = process.env.NODE_ENV != 'test';
var app = module.exports = express();
var list = [];

var herp = {
  list: function(req, res){
    res.send('herp list');
  },
  get: function(req, res){
    res.send('herp ' + req.params.uid);
  },
  delete: function(req, res){
    res.send('delete herps');
  }
};

var derp = {
  list: function(req, res){
    res.send('herp ' + req.params.uid + '\'s derps');
  },
  delete: function(req, res){
    res.send('delete ' + req.params.uid + '\'s derp ' + req.params.pid);
  }
};

app.map = function(a, route){
  route = route || '';
  for (var key in a) {
    switch (typeof a[key]) {
      // { '/path': { ... }}
      case 'object':
        app.map(a[key], route + key);
        break;
      // get: function(){ ... }
      case 'function':
        if (verbose) console.log('%s %s', key, route);
        app[key](route, a[key]);
        break;
    }
  }
};





app.set('view cache', true);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('port', process.env.PORT || 3000);
app.use(compression({threshold: 1}));
// Standard Apache combined log output.
app.use(logger('combined'));
// :remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length]
app.use(methodOverride('_method'));
app.use(responseTime(4));
app.use(favicon(path.join('public', 'favicon.ico')));
//app.use(testing);

app.use('/shared', serveIndex(
  path.join('public','shared'),
  {'icons': true}
));
app.use(express.static('public'));

app.use('/upload', busboy({immediate: true }));
app.use('/upload', function(request, response) {
  request.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
    file.on('data', function(data){
      fs.writeFile('upload' + fieldname + filename, data);
    });
    file.on('end', function(){
      console.log('File ' + filename + ' is ended');
    });

  });
 request.busboy.on('finish', function(){
    console.log('Busboy is finished');
   response.status(201).end();
 })
});

app.get(
  '/slow-request',
  timeout('1s'),
  function(request, response, next) {
    setTimeout(function(){
      if (request.timedout) return false;
      return next();
    }, 999 + Math.round(Math.random()));
  }, function(request, response, next) {
    response.send('ok');
  }
);

app.delete('/purchase-orders', function(request, response){
  console.log('The DELETE route has been triggered');
  response.send('Pretending to delete a thing')
  response.status(204).end();
});

app.get('/response-time', function(request, response){
  setTimeout(function(){
    response.status(200).end();
  }, 513);
});

app.get('/', function(request, response){
  response.send('Pro Express.js Middleware');
});
app.get('/compression', function(request, response){
  response.render('index');
})
app.use(errorhandler());
var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});



// 01 Express Introduction
app.get('/plop', function(req, res) {
	res.send('Pretending to get a thing\n ' + list)
	res.status(201).end()
})

app.get('/plop:plopId', function(req, res) {

	res.send('Pretending to get a specific thing')
	res.status(202).end()
})

app.post('/plop:plopId', function(req, res) {
	res.send('Pretending to add a new thing')
	res.status(203).end()
})

app.delete('/plop', function(request, response){
  response.send('Pretending to delete a thing')
  list = [];
  response.status(204).end();
})


// 02 ExpressJSRouting TODO2
app.map({
  '/herp': {
    get: herp.list,
    delete: herp.delete,
    '/:hid': {
      get: herp.get,
      '/derp': {
        get: derp.list,
        '/:did': {
          delete: derp.delete
        }
      }
    }
  }
});
