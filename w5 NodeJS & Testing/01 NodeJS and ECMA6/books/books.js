

var request = require('request')

var bookList = []

var bookInList = bookId => {
	if (bookList.includes(bookId)) {
		return true
	}
	return false
}
var validateBookId = bookId => {
	/*
  try {
    const result = exports.describe(bookId, err)
    console.log(result)
  } catch (err) {
    console.log(err)
  } finally {
    console.log('the list contains '+exports.bookCount()+' books')
  }
  */
  return true
}

/* The standard pattern for asynchronous callbacks is for the first parameter to be the error, this should be null if no error is thrown with the second parameter being the data. */
exports.search = (query, callback) => {
  if (typeof query !== 'string' || query.length === 0) {
    callback(new Error('missing query parameter'))
  }
  const url = 'https://www.googleapis.com/books/v1/volumes'
  const query_string = {q: query, maxResults: 3, fields: 'items(id,volumeInfo(title,authors))'}
  request.get({url: url, qs: query_string}, (err, res, body) => {
    if (err) {
      callback(new Error('error making google books request'))
    }
    const json = JSON.parse(body)
    const items = json.items
    if (items === undefined) {
      //console.log('found undefined property')
      callback(new Error('no books found matching search'))
      return
    }
    const books = items.map( element => {
      return {id:element.id, title:element.volumeInfo.title, authors:element.volumeInfo.authors}
    })
    /* the first callback parameter is the error, which in this case will be null, the second parameter is the data returned. */
    callback(null, books)
  })
}

// Not entirely sure how exactly you want this to differ from search(), but I changed some stuff.
exports.describe = (query, callback) => {
  if (typeof query !== 'string' || query.length === 0) {
    callback(new Error('missing query parameter'))
  }
  const url = 'https://www.googleapis.com/books/v1/volumes?q=tBbsAgAAQBAJ'
  const query_string = {q: query, maxResults: 1,
  	fields: 'items(id,volumeInfo(title,subtitle,authors,publisher,publishedDate,description))'}
  request.get({url: url, qs: query_string}, (err, res, body) => {
    if (err) {
      callback(new Error("Something got f***ed"))
    }
    const json = JSON.parse(body)
    const items = json.items
    if (items === undefined) {
      //console.log('found undefined property')
      callback(new Error("There's no such book, you fool."))
      return
    }
    const books = items.map( element => {
      return {
	      id:element.id,
	      title:element.volumeInfo.title,
	      subtitle:element.volumeInfo.subtitle,
	      authors:element.volumeInfo.authors,
	      publisher:element.volumeInfo.publisher,
	      publishedDate:element.volumeInfo.publishedDate,
	      description:element.volumeInfo.description,
      }
    })
    callback(null, books)
  })
}

/* a synchronous function will either return data or throw an error */
exports.add = bookId => {
	if (!validateBookId(bookId)) {
		throw ("That book doesn't exist.")
	}
  if (bookId.length != 12) {
    /* this throws a user-defined exception. */
    throw('bookId should be 12 character long')
  }
  if (bookList.indexOf(bookId) != -1) {
    throw('book has already been added to the list')
  }
  bookList.push(bookId)
  //console.log(bookList.length)
  return 'book '+bookId+' added'
}

exports.remove = bookId => {
	if (!bookInList(bookId)) {
		throw("There's no such book, you fool.")
	}
	bookList.splice(bookList.indexOf(bookId), 1)
	return "book " + bookId + " removed"
}

exports.bookCount = () => {
  return bookList.length
}