
"use strict"







//***************************************************v
/*
TODO2: "Locate any errors in the script and fix"
Uhhhhhhhhhh... what errors? The left margin isn't showing any. Not even warnings.
I tried to googling it, just in case it was a bug or a setting or something but I can't find anything.
And if I intentionally make undefined variables or incorrect number of brackets, it points out THOSE just fine.
So the only conclusion I can draw, is that there are no errors or warnings to begin with?
*/
//***************************************************^



const readline = require('readline-sync')
let cost = 0;
const type = String(readline.question('membership type (basic, premium, gold): ')).trim()
const years = Number(readline.question('years required: '))

try {
	if (Number.isNaN(years) || years < 1) {
	throw 'invalid number of years requested'
	}
	switch (type) {
		case 'basic':
			cost = 10.00
			break
		case 'premium':
			cost = 15.00
			break
		case 'gold':
			cost = 20.00
			break
		default:
			throw 'invalid membership type'
	}
	cost *= years
	if (years > 1) {
		cost *= 0.8
	}
	console.log("membership cost is £"+cost)
} catch(err) {
console.log(err)
}
